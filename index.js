const cron = require('node-cron');
const shell = require('shelljs');


const restartAPI = () => {
    const data = new Date();
    console.log(`Restart API ${data.getHours()}:${data.getMinutes()}:${data.getSeconds()}`);
    shell.exec(`nodemon`);
    shell.exit(1);
}


cron.schedule('0 11,13,14 * * *', restartAPI, { scheduled: true });

/* 
    Referencias
    https://www.npmjs.com/package/node-cron
    https://www.npmjs.com/package/shelljs
*/
